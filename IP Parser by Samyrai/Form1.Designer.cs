﻿namespace IP_Parser_by_Samyrai
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonDiap = new System.Windows.Forms.RadioButton();
            this.radioButtonIP = new System.Windows.Forms.RadioButton();
            this.buttonStart = new System.Windows.Forms.Button();
            this.numericUpDownTimeout = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLink = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxProxies = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownThreads = new System.Windows.Forms.NumericUpDown();
            this.buttonStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownThreads)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButtonDiap
            // 
            this.radioButtonDiap.AutoSize = true;
            this.radioButtonDiap.Checked = true;
            this.radioButtonDiap.Location = new System.Drawing.Point(12, 51);
            this.radioButtonDiap.Name = "radioButtonDiap";
            this.radioButtonDiap.Size = new System.Drawing.Size(84, 17);
            this.radioButtonDiap.TabIndex = 2;
            this.radioButtonDiap.TabStop = true;
            this.radioButtonDiap.Text = "Диапазоны";
            this.radioButtonDiap.UseVisualStyleBackColor = true;
            this.radioButtonDiap.CheckedChanged += new System.EventHandler(this.radioButtonDiap_CheckedChanged);
            // 
            // radioButtonIP
            // 
            this.radioButtonIP.AutoSize = true;
            this.radioButtonIP.Location = new System.Drawing.Point(12, 74);
            this.radioButtonIP.Name = "radioButtonIP";
            this.radioButtonIP.Size = new System.Drawing.Size(35, 17);
            this.radioButtonIP.TabIndex = 3;
            this.radioButtonIP.Text = "IP";
            this.radioButtonIP.UseVisualStyleBackColor = true;
            this.radioButtonIP.CheckedChanged += new System.EventHandler(this.radioButtonIP_CheckedChanged);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(8, 136);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(261, 23);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "Старт";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // numericUpDownTimeout
            // 
            this.numericUpDownTimeout.Location = new System.Drawing.Point(12, 110);
            this.numericUpDownTimeout.Name = "numericUpDownTimeout";
            this.numericUpDownTimeout.Size = new System.Drawing.Size(46, 20);
            this.numericUpDownTimeout.TabIndex = 5;
            this.numericUpDownTimeout.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Таймаут";
            // 
            // textBoxLink
            // 
            this.textBoxLink.Enabled = false;
            this.textBoxLink.Location = new System.Drawing.Point(8, 25);
            this.textBoxLink.Name = "textBoxLink";
            this.textBoxLink.Size = new System.Drawing.Size(261, 20);
            this.textBoxLink.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Префикс";
            // 
            // comboBoxProxies
            // 
            this.comboBoxProxies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProxies.FormattingEnabled = true;
            this.comboBoxProxies.Items.AddRange(new object[] {
            "HTTP/S",
            "SOCKS4",
            "SOCKS5"});
            this.comboBoxProxies.Location = new System.Drawing.Point(148, 50);
            this.comboBoxProxies.Name = "comboBoxProxies";
            this.comboBoxProxies.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProxies.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(148, 78);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Загрузить прокси";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Потоки";
            // 
            // numericUpDownThreads
            // 
            this.numericUpDownThreads.Location = new System.Drawing.Point(75, 110);
            this.numericUpDownThreads.Name = "numericUpDownThreads";
            this.numericUpDownThreads.Size = new System.Drawing.Size(46, 20);
            this.numericUpDownThreads.TabIndex = 11;
            this.numericUpDownThreads.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(8, 166);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(261, 23);
            this.buttonStop.TabIndex = 13;
            this.buttonStop.Text = "Стоп";
            this.buttonStop.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 195);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownThreads);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxProxies);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxLink);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownTimeout);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.radioButtonIP);
            this.Controls.Add(this.radioButtonDiap);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IP Parser by Samyrai";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownThreads)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton radioButtonDiap;
        private System.Windows.Forms.RadioButton radioButtonIP;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.NumericUpDown numericUpDownTimeout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxProxies;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownThreads;
        private System.Windows.Forms.Button buttonStop;
    }
}

