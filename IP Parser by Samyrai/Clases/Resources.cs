﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using IP_Parser_by_Samyrai.Clases;

class Resources
{

    public static bool LoadProxies()
    {
        using (OpenFileDialog opd = new OpenFileDialog()) {
            if (opd.ShowDialog() == DialogResult.OK) {
                List<string> source = File.ReadAllLines(opd.FileName).ToList();
                source = source.Distinct().ToList();
                GVars.Proxies = source.Distinct().ToArray();
                return true;
            }
        }
        return false;
    }

    public static string GetProxy() {
        lock (GVars.Lock) {
            if (GVars.ProxyIndex >= GVars.Proxies.Length)
                GVars.ProxyIndex = 0;

            GVars.ProxyIndex++;

            return GVars.Proxies[GVars.ProxyIndex];
        }
    }



    

    public static int GetPages() {

        lock (GVars.Lock)
        {
           int index =  GVars.PagesIndex++;
            return index;
        }        

    }

}