﻿using System.IO;
using xNet;

namespace IP_Parser_by_Samyrai.Clases
{
    class Checker
    {
        public static string ParseDiap(string link)
        {

            try
            {
                using (HttpRequest req = new HttpRequest())
                {

                    while (true)
                    {
                        try
                        {
                            string prox = Resources.GetProxy();
                            switch (GVars.ProxyType)
                            {
                                case 0:
                                    req.Proxy = HttpProxyClient.Parse(prox);
                                    break;
                                case 1:
                                    req.Proxy = Socks4ProxyClient.Parse(prox);
                                    break;
                                case 2:
                                    req.Proxy = Socks5ProxyClient.Parse(prox);

                                    break;

                                default:
                                    req.Proxy = HttpProxyClient.Parse(prox);
                                    break;
                            }

                            req.Proxy.ConnectTimeout = GVars.TimeOut * 1000;
                            req.Proxy.ReadWriteTimeout = req.Proxy.ConnectTimeout;
                            req.Cookies = new CookieDictionary();
                            req.UserAgent = Http.ChromeUserAgent();

                            string s = req.Get(link).ToString();
                            string getPagesCount = s.Substring("true, '1', '", "', {sortCol: '4',"); //, '13156', {sortCol: '4',
                            if (getPagesCount == "")
                            {
                                getPagesCount = "13000";
                            }
                            for (int i = 0; i < 50; i++)
                            {
                                
                               // s = req.Get(link).ToString();

                                try
                                {
                                    string g = s.Substrings("<td class='row_name'><a   href='/", "' >")[i];
                                    string d = g.Split('/')[3].Replace("_", "-") + "\r\n";
                                    File.AppendAllText("GoodDiap.txt", d);
                                }
                                catch { }
                                
                            }
                            
                            return "";


                        }
                        catch { GVars.Error++; }

                    }
                }
            }

            catch { return "error"; }

        }
        public static string ParseIP(string link)
        {

            try
            {
                using (HttpRequest req = new HttpRequest())
                {

                    while (true)
                    {
                        try
                        {
                            string prox = Resources.GetProxy();
                            switch (GVars.ProxyType)
                            {
                                case 0:
                                    req.Proxy = HttpProxyClient.Parse(prox);
                                    break;
                                case 1:
                                    req.Proxy = Socks4ProxyClient.Parse(prox);
                                    break;
                                case 2:
                                    req.Proxy = Socks5ProxyClient.Parse(prox);

                                    break;

                                default:
                                    req.Proxy = HttpProxyClient.Parse(prox);
                                    break;
                            }

                            req.Proxy.ConnectTimeout = GVars.TimeOut * 1000;
                            req.Proxy.ReadWriteTimeout = req.Proxy.ConnectTimeout;
                            req.Cookies = new CookieDictionary();
                            req.UserAgent = Http.ChromeUserAgent();

                            string s = req.Get(link).ToString();
                          //  File.WriteAllText("page.txt", s);
                            string getPagesCount = s.Substring("true, '1', '", "', {sortCol: '7',"); //true, '1', '919183', {sortCol: '7'
                            if (getPagesCount == "")
                            {
                                getPagesCount = "13000";
                            }

                            for (int i = 0; i < 50; i++)
                            {

                                try
                                {
                                    string g = s.Substrings("<td class='row_name'><a alt=\"", "\"")[i];  ///98.126.242.178'>More Details
                                    string d = g;
                                    File.AppendAllText("GoodIP.txt", d + "\r\n");
                                   // MessageBox.Show(d);
                                }
                                catch { }

                            }

                            return "norm";


                        }
                        catch { GVars.Error++; }

                    }
                }
            }

            catch { return "error"; }

        }
    }
}
