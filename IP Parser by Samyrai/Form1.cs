﻿using IP_Parser_by_Samyrai.Clases;
using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using xNet;

namespace IP_Parser_by_Samyrai
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static string _link = "";
        

        public static int _getPagesCount = 0;

        
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if(GVars.Proxies == null)
            {
                MessageBox.Show("Вы забыли прокси", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(textBoxLink.Text == "" && radioButtonIP.Checked)
            {
                MessageBox.Show("Вы забыли префикс", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            GVars.ThreadCount = (int)numericUpDownThreads.Value;
           // GVars.ThreadCount = 50;
            _link = textBoxLink.Text;
            _getPagesCount = GetPagesCount();
            
            GVars.Working = true;


            GVars.TimeOut = (int)numericUpDownTimeout.Value;
            ThreadPool.SetMinThreads(GVars.ThreadCount + 2, GVars.ThreadCount + 2);
            ThreadPool.SetMaxThreads(GVars.ThreadCount + 2, GVars.ThreadCount + 2);

            for (int i = 0; i < GVars.ThreadCount; i++)
            {
                if(radioButtonDiap.Checked)
                ThreadPool.QueueUserWorkItem(DoWorkDiap);
                else
                    ThreadPool.QueueUserWorkItem(DoWorkIP);

            }

            textBoxLink.Enabled = !GVars.Working;
            radioButtonDiap.Enabled = !GVars.Working;
            radioButtonIP.Enabled = !GVars.Working;
            numericUpDownTimeout.Enabled = !GVars.Working;
            numericUpDownThreads.Enabled = !GVars.Working;
            buttonStart.Enabled = !GVars.Working;
            button1.Enabled = !GVars.Working;
            comboBoxProxies.Enabled = !GVars.Working;


        }

        



        void DoWorkDiap(object state)
        {
            int accs = 1;
            while (GVars.Working)
            {
                
                try
                {
                    

                    lock (GVars.Lock)
                    {
                        
                        if (accs >= _getPagesCount)
                        {
                            MessageBox.Show("готово","", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            return;
                        }
                        
                    }

                    accs = Resources.GetPages();

                    string res = Checker.ParseDiap("https://myip.ms/browse/ip_ranges/" + accs + "/IP_Ranges_by_Owner");




                        lock (GVars.Lock)
                        {
                            

                            if (res == "error")
                            {
                                GVars.Error++;
                            }

                            else
                            {
                            

                            GVars.Done++;
                                GVars.Good++;

                            }

                        }
                    }
                
                catch { }
            }
            //MessageBox.Show("Закончено");
        }

        void DoWorkIP(object state)
        {
            int accs = 1;
            while (GVars.Working)
            {

                try
                {


                    lock (GVars.Lock)
                    {

                        if (accs >= _getPagesCount)
                        {

                            return;
                        }

                    }
                    accs = Resources.GetPages();

                    string res = Checker.ParseIP("https://myip.ms/browse/myip/" + accs.ToString() + "/" + _link);

                    


                    lock (GVars.Lock)
                    {


                        if (res == "error")
                        {
                            GVars.Error++;
                        }

                        else
                        {
                         //   accs = Resources.GetPages();

                            GVars.Done++;
                            GVars.Good++;

                        }

                    }
                }

                catch { }
            }
            //MessageBox.Show("Закончено");
        }

        private int GetPagesCount()
        {
            if(radioButtonDiap.Checked)
            {
                File.WriteAllText("GoodDiap.txt", "");
                using (HttpRequest req = new HttpRequest())
                {
                    req.Cookies = new CookieDictionary();
                    //   req.KeepAlive = true;
                    //  req.IgnoreProtocolErrors = true;
                    req.UserAgent = Http.ChromeUserAgent();

                    string s = req.Get("https://myip.ms/browse/ip_ranges/1/IP_Ranges_by_Owner").ToString();
                    //  , '13156', {sortCol:
                    string getPagesCount = s.Substring("true, '1', '", "', {sortCol: '4',"); //, '13156', {sortCol: '4',
                    if (getPagesCount == "")
                    {
                        getPagesCount = "13000";
                    }
                    return Convert.ToInt32(getPagesCount);

                }
            }
            else
            {
                File.WriteAllText("GoodIP.txt", "");
                using (HttpRequest req = new HttpRequest())
                {
                    req.Cookies = new CookieDictionary();
                    //   req.KeepAlive = true;
                    //  req.IgnoreProtocolErrors = true;
                    req.UserAgent = Http.ChromeUserAgent();

                    string s = req.Get("https://myip.ms/browse/myip/1/" + _link).ToString();
                    //  , '13156', {sortCol:
                    string getPagesCount = s.Substring("true, '1', '", "', {sortCol: '7',"); //, '13156', {sortCol: '4',
                    if (getPagesCount == "")
                    {
                        getPagesCount = "13000";
                    }
                    return Convert.ToInt32(getPagesCount);

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            comboBoxProxies.SelectedIndex = 0;
            GVars.ProxyType = 0;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Resources.LoadProxies())
            {
                MessageBox.Show("Успешно загружено " + GVars.Proxies.Length + " прокси");
            }
        }

        private void radioButtonIP_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonIP.Checked)
                textBoxLink.Enabled = true;
        }

        private void radioButtonDiap_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonDiap.Checked)
            {
                textBoxLink.Enabled = false;
                textBoxLink.Text = "";
            }
                
        }
    }
}
